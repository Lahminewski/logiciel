---
title: "Ma page de recommandations"
order: 0
in_menu: true
---
Voici une petite liste de logiciel libre que je recommande.

# Mes logiciels libres pour randonner

## Pour les podcat

  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/AntennaPod.png">
    </div>
    <div>
      <h2>AntennaPod</h2>
      <p>Un gestionnaire de Podcast pour Android.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/antennapod.html">Vers la notice Framalibre</a>
        <a href="http://antennapod.org/">Vers le site</a>
      </div>
    </div>
  </article> 

Pour le montage audio en géneral.
 <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Ardour.png">
    </div>
    <div>
      <h2>Ardour</h2>
      <p>La station audio-numérique libre de référence, pour dire au revoir à Cubase et Pro Tools !</p>
      <div>
        <a href="https://framalibre.org/notices/ardour.html">Vers la notice Framalibre</a>
        <a href="https://ardour.org/">Vers le site</a>
      </div>
    </div>
  </article> 

## Pour le reste ...

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Nextcloud.png">
    </div>
    <div>
      <h2>Nextcloud</h2>
      <p>Nextcloud est une plate-forme auto-hébergée de services de stockage et de diverses applications dans les nuages</p>
      <div>
        <a href="https://framalibre.org/notices/nextcloud.html">Vers la notice Framalibre</a>
        <a href="https://nextcloud.com/">Vers le site</a>
      </div>
    </div>
  </article> 

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Element.io%20(ex%20Riot.im).png">
    </div>
    <div>
      <h2>Element (ex Riot)</h2>
      <p>Element rassemble toutes vos conversations et intégrations applicatives en une seule application.</p>
      <div>
        <a href="https://framalibre.org/notices/element-ex-riot.html">Vers la notice Framalibre</a>
        <a href="https://element.io">Vers le site</a>
      </div>
    </div>
  </article> 


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Scribouilli.png">
    </div>
    <div>
      <h2>Scribouilli</h2>
      <p>Créer votre petit site facilement !</p>
      <div>
        <a href="https://framalibre.org/notices/scribouilli.html">Vers la notice Framalibre</a>
        <a href="https://scribouilli.org/">Vers le site</a>
      </div>
    </div>
  </article> 